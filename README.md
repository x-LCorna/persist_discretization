# Persist: Time Series Discretization
Time Series discretization approach originally proposed by Fabian Moerchen et Alfred Ultsch (2005) based on the notion of persistence.

## Overview
Given a time series, Persist outputs a set of breakpoints creating intervals in the value space. Each interval is associated with a symbol that will replace the numerical values falling within it in the discretized time series. To select these breakpoints, Persist looks for solutions that will create persisting symbols in the time space.
In this re-implementation of the Persist algorithm, the metric used to compute the persistence score can be the Wasserstein distance or the Kullback-Leibler divergence. 
Two strategies of breakpoints initializations are also proposed, an equal-width binning or an equal-frequency binning.

📚 See following publications for more information:
- Persistence-Based Discretization for Learning Discrete Event Systems from Time Series. Lénaïg Cornanguer, Christine Largouët, Laurence Rozé, Alexandre Termier. In AAAI 2023 Workshop "When Machine Learning meets Dynamical Systems: Theory and Applications" (MLmDS 2023).
- Optimizing Time Series Discretization for Knowledge Discovery. Fabian Moerchen, Alfred Ultsch. In Proceedings The Eleventh ACM SIGKDD International Conference on Knowledge Discovery and Data Mining (2005).

Persist original implementation (in MATLAB) is available here: https://www.mybytes.de/#code

## Usage
### ⚙️ Arguments
* x (array): time series 
* kmin (int, default=2): minimal number of breakpoints
* kmax (int, default=10): maximal number of breakpoints
* divergence (str, default="w"): metric to compute the persistence score (kl: Kullback-Leibler divergence / w: Wasserstein distance)
* skip (array, default=np.array([4, 4])): number of first and last candidate breakpoints to skip to avoid extreme breakpoint values
* candidates (str, default="EW"): candidate breakpoints initialization strategy (EW: equal-width binning / EF: equal-frequency binning)

### 📝 Example
With Chinatown traffic dataset (available here: http://www.timeseriesclassification.com/description.php?Dataset=Chinatown):

```python
import numpy as np
from scipy.io import arff
from Persist import Persist

# Breakpoints creation
f_train = "Chinatown_TRAIN.arff"
df_train, meta = arff.loadarff(f_train)
ts = np.array(df_train[list(meta._attributes.keys())[:-1]].tolist())
p = Persist(np.array(ts), divergence="w", candidates="EW")
bkpts = [b for b in p.bins[np.argmax(p.pscores)] if not np.isnan(b)] # best breakpoints

# Vizualize breakpoints on an instance of time series
import matplotlib.pyplot as plt

plt.plot(ts[0])
for b in bkpts:
    plt.axhline(b, color="red")
```

